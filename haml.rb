# haml
gem 'haml-rails'
gem 'erb2haml', require: false

after_bundle do
  # erb to haml
  rake 'haml:erb2haml'
end

