# slim
gem 'slim-rails'

gem_group(:development) do
  # 2024/05/24現在、gemが更新されていないので、GitHubから取得するようにしている
  # gem 'html2slim', require: false
  # gem 'html2slim-ruby3', require: false
  gem 'html2slim', github: 'slim-template/html2slim', require: false
end

after_bundle do
  # erb to slim
  run('find app -name *.html.erb -exec erb2slim {} \; -exec rm {} \;')
end
