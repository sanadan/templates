gem 'webpacker'

gem_group :development do
  gem 'foreman', require: false
end

file 'Procfile', <<-PROCFILE
rails: bin/rails s
browser-sync: ruby -e "sleep 1 until File.exists?('tmp/pids/server.pid')" && browser-sync --config bs-config.js start
webpack: bin/webpack-dev-server
PROCFILE

file 'bs-config.js', <<-BSCONFIGJS
module.exports = {
  files: [ "app/assets/stylesheets/*.css.*", "app/views/**/*.html.*", "app/assets/javascripts/**/*.js.*" ],  // 変更を監視するファイル
  proxy: "localhost:5000",
  port: 3000,
  snippetOptions: {
    rule: {
      match: /<\head>/i,
      fn: function ( snippet, match ) {
        return snippet + match ;
      }
    }
  }
} ;
BSCONFIGJS

file 'live', <<-LIVE
#!/bin/sh
foreman start
LIVE

after_bundle do
  # webpackをインストール
  run( 'bin/rails webpacker:install' )

  # BrowserSync起動スクリプト準備
  run( 'chmod +x live' )
end

