# opal
gem 'opal-rails'

after_bundle do
  run 'rm app/assets/javascripts/application.js'
  file 'app/assets/javascripts/application.js.rb', <<-EOF
require 'opal'
require 'rails-ujs'
require 'turbolinks'
require 'jquery'
require 'jquery_ujs'
require 'opal-jquery'
require 'native'
require_tree '.'
  EOF
end

