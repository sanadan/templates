# frozen_string_literal: true

# gem
gem 'devise'
gem 'devise-i18n'
after_bundle do
  generate 'devise:install'
  generate 'devise user'
  generate 'devise:views users'
  run 'find app -name “*.erb” -exec erb2slim {} \; -exec rm {} \;'
  environment "config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }", env: 'development'
  environment "config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }", env: 'production'
end

# 10. vi app/views/layouts/application.rb
# 11. <p class="notice"><%= notice %></p>
# 12. <p class="alert"><%= alert %></p>
# 16. vi db/seeds.rb

