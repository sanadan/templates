# frozen_string_literal: true

generate :controller, 'home index'
route "root to: 'home#index'"

