gem_group :development do
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
end

file '.rubocop.yml', <<~RUBOCOP
  require:
    - rubocop-performance
    - rubocop-rails

  AllCops:
    NewCops: enable
RUBOCOP

run('bundle install')
