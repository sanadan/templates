# Railsのテンプレート集
## 使い方
### 新規作成時
`rails new sample -m https://gitlab.com/sanadan/templates/raw/master/slim.rb`

### 既存に追加時
`rails app:template LOCATION=https://gitlab.com/sanadan/templates/raw/master/slim.rb`

## 有効になるもの
### slim.rb
* slim

### haml.rb
* haml
