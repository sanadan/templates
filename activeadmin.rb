# frozen_string_literal: true

# gem
gem 'activeadmin'

after_bundle do
  generate 'active_admin:install'
end

